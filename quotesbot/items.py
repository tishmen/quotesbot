'''items.py'''

from scrapy import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Identity


class BaseItem(Item):

    '''Base item.'''

    index = Field()
    uid = Field()


class JobItem(BaseItem):

    '''Job item.'''

    project = Field()
    spider = Field()
    args = Field()
    status = Field()
    starttime = Field()
    endtime = Field()


class RequestItem(BaseItem):

    '''Request item.'''

    jobid = Field()
    url = Field()
    method = Field()
    headers = Field()
    cookies = Field()
    body = Field()


class ResponseItem(BaseItem):

    '''Response item.'''

    jobid = Field()
    request = Field()
    url = Field()
    status = Field()
    headers = Field()
    size = Field()


class QuoteItem(BaseItem):

    '''Quote item.'''

    jobid = Field()
    url = Field()
    text = Field()
    author = Field()
    tags = Field()


class BaseItemLoader(ItemLoader):

    '''Base item loader.'''

    default_output_processor = TakeFirst()


class JobItemLoader(BaseItemLoader):

    '''Job item loader.'''

    default_item_class = JobItem


class RequestItemLoader(BaseItemLoader):

    '''Request item loader.'''

    default_item_class = RequestItem


class ResponseItemLoader(BaseItemLoader):

    '''Response item loader.'''

    default_item_class = ResponseItem


class QuoteItemLoader(BaseItemLoader):

    '''Quote item loader.'''

    default_item_class = QuoteItem
    tags_out = Identity()
