'''extensions.py'''

import os
import uuid

from datetime import datetime

from scrapy import signals

from quotesbot.items import JobItemLoader, RequestItemLoader, ResponseItemLoader


class BaseExtension(object):

    '''Base functionality for feeding items to crawler engine.'''

    def __init__(self, crawler):
        '''Set crawler and job id.'''
        self.crawler = crawler
        self.jobid = os.getenv('SCRAPY_JOB')

    @property
    def uid(self):
        '''Return random 36 character item id.'''
        return str(uuid.uuid4())

    def add_index(self, item):
        '''Populate index field on item. Used by Elasticsearch.'''
        index = type(item).__name__.lower().replace('item', '')
        item.setdefault('index', index)
        return item

    def add_item(self, item):
        '''
        Feed items to engine using protected _process_spidermw_output from
        scrapy.core.scraper. Cannot be used alongside pipelines or middlewares
        as this method will refeed items into the engine.
        https://github.com/scrapy/scrapy/issues/1915
        '''
        self.crawler.engine.scraper._process_spidermw_output(
            self.add_index(item), None, 'item extension', self.crawler.spider
        )


class JobExtension(BaseExtension):

    '''Populate job item and feed to crawler engine.'''

    def __init__(self, crawler):
        '''Set crawler in base and starttime to None.'''
        super(JobExtension, self).__init__(crawler)
        self.starttime = None

    @classmethod
    def from_crawler(cls, crawler):
        '''
        Pass crawler to constructor and connect spider_opened and spider_idle
        signals.
        '''
        ext = cls(crawler)
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(ext.spider_idle, signal=signals.spider_idle)
        return ext

    @property
    def base_loader(self):
        '''Populate common job fields and return item loader.'''
        loader = JobItemLoader()
        loader.add_value('uid', self.jobid)
        loader.add_value('project', self.crawler.settings.get('BOT_NAME'))
        loader.add_value('spider', self.crawler.spider.name)
        loader.add_value('args', getattr(self.crawler.spider, 'args', {}))
        return loader

    def spider_opened(self, *args, **kwargs):
        '''
        Handle spider_opened signal. Set status to started. Populate start time
        field.
        '''
        self.starttime = datetime.now().isoformat()
        loader = self.base_loader
        loader.add_value('status', 'started')
        loader.add_value('starttime', self.starttime)
        self.add_item(loader.load_item())

    def spider_idle(self, *args, **kwargs):
        '''
        Handle spider_idle signal. Set status to stopped. Populate start time
        and end time field.
        '''
        loader = self.base_loader
        loader.add_value('status', 'stopped')
        loader.add_value('starttime', self.starttime)
        loader.add_value('endtime', datetime.now().isoformat())
        self.add_item(loader.load_item())


class RequestResponseExtension(BaseExtension):

    '''Populate request and response items and feed to crawler engine.'''

    def __init__(self, crawler):
        '''Set crawler in base.'''
        super(RequestResponseExtension, self).__init__(crawler)

    @classmethod
    def from_crawler(cls, crawler):
        '''Pass crawler to constructor and connect response_received signal.'''
        ext = cls(crawler)
        crawler.signals.connect(
            ext.response_received, signal=signals.response_received
        )
        return ext

    def response_received(self, response, request, *args, **kwargs):
        '''Handle response_received signal.'''
        uid = self.uid
        self.add_item(self.process_request(request, uid))
        self.add_item(self.process_response(response, uid))

    def process_request(self, request, uid):
        '''Load request item.'''
        loader = RequestItemLoader()
        loader.add_value('jobid', self.jobid)
        loader.add_value('uid', uid)
        loader.add_value('url', request.url)
        loader.add_value('method', request.method)
        loader.add_value('headers', request.headers.to_unicode_dict())
        loader.add_value('cookies', request.cookies)
        loader.add_value('body', request.body.decode(request.encoding))
        return loader.load_item()

    def process_response(self, response, uid):
        '''Load response item.'''
        loader = ResponseItemLoader()
        loader.add_value('jobid', self.jobid)
        loader.add_value('uid', self.uid)
        loader.add_value('request', uid)
        loader.add_value('url', response.url)
        loader.add_value('status', response.status)
        loader.add_value('headers', response.headers.to_unicode_dict())
        loader.add_value('size', len(response.body))
        return loader.load_item()
