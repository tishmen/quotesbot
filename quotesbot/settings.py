'''settings.py'''


BOT_NAME = 'quotesbot'
SPIDER_MODULES = ['quotesbot.spiders']
LOG_LEVEL = 'DEBUG'
MYEXT_ENABLED = True
EXTENSIONS = {
    'quotesbot.extensions.JobExtension': 100,
    'quotesbot.extensions.RequestResponseExtension': 100
}
