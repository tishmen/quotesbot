'''quotes.py'''

import os
import uuid

from scrapy import Spider, Request
from scrapy.exceptions import NotConfigured

from quotesbot.items import QuoteItemLoader


class QuotesSpider(Spider):

    '''Quotes spider.'''

    name = 'quotes'

    def __init__(self, *args, **kwargs):
        '''Set args.'''
        super(QuotesSpider, self).__init__(*args, **kwargs)
        self.args = kwargs

    def start_requests(self):
        '''Append tag query to start url.'''
        query = self.args.get('query')
        if not query:
            raise NotConfigured
        url = 'http://quotes.toscrape.com/tag/{}/'.format(query)
        yield Request(url)

    def process_item(self, quote, response):
        '''Load quote item.'''
        loader = QuoteItemLoader(selector=quote)
        loader.add_value('index', 'quote')
        loader.add_value('jobid', os.getenv('SCRAPY_JOB'))
        loader.add_value('uid', str(uuid.uuid4()))
        loader.add_value('url', response.url)
        loader.add_xpath('text', './span[@class="text"]/text()')
        loader.add_xpath('author', './/small[@class="author"]/text()')
        loader.add_xpath('tags', './/div[@class="tags"]/a[@class="tag"]/text()')
        return loader.load_item()

    def parse(self, response):
        '''
        Parse response and follow next page.

        Spider contracts:

        @url http://quotes.toscrape.com/
        @returns items 10
        @returns requests 1
        '''
        for quote in response.xpath('//div[@class="quote"]'):
            yield self.process_item(quote, response)
        next_url = response.xpath('//li[@class="next"]/a/@href').extract_first()
        if next_url:
            yield Request(response.urljoin(next_url))
