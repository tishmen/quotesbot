#!/bin/bash


# Check root

if [[ $EUID > 0 ]]; then
  echo "Please run as root/sudo"
  exit 1
fi

# Install packages

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" |
  tee -a /etc/apt/sources.list.d/elastic-6.x.list
apt-get update && apt-get install -y default-jre elasticsearch kibana filebeat \
  nodejs nodejs-legacy npm supervisor python3 python3-dev python3-pip \
  libxml2-dev libxslt1-dev zlib1g-dev libffi-dev libssl-dev

# Setup elastic

cp -rf kibana.yml /etc/kibana/kibana.yml
cp -rf filebeat.yml /etc/filebeat/filebeat.yml
systemctl daemon-reload
systemctl enable elasticsearch.service
systemctl start elasticsearch.service
systemctl enable kibana.service
systemctl start kibana.service
systemctl enable filebeat.service
systemctl start filebeat.service

# Setup scrapy

pip3 install -r requirements.txt
cp scrapyd.conf /etc/supervisor/conf.d/scrapyd.conf
supervisorctl reread && supervisorctl update
scrapyd-deploy


# Load kibana

curl -f -XPOST -H "Content-Type: application/json" -H "kbn-xsrf: anything" \
  "http://localhost:5601/api/saved_objects/index-pattern/filebeat-*" \
  -d "{\"attributes\":{\"title\":\"filebeat-*\",\"timeFieldName\":\"@timestamp\"}}"
curl -XPOST -H "Content-Type: application/json" -H "kbn-xsrf: anything" \
  "http://localhost:5601/api/kibana/settings/defaultIndex" \
  -d "{\"value\":\"filebeat-*\"}"

# npm install elasticdump -g
# elasticdump --input=kibana.json --output=http://localhost:9200/.kibana \
#   --type=data --headers='{"Content-Type": "application/json"}'

# Dump kibana

# elasticdump --input=http://localhost:9200/.kibana / --output=kibana.json /
#   --type=data --headers='{"Content-Type": "application/json"}'
